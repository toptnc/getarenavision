# getarenavision
Simple desktop script to scrape arenavision channels and generate a playlist file.

# Requirements
This script requires various tools to work.

* curl
* html2text
* awk
* sed
* yad (Desktop menus)
* Acestream player (acestreamplayer)
 
# Usage
This script doesn't need any arguments. It will scrape http://arenavision.in/guide and will write two files in your DESKTOP dir.

* arenavision-guide.txt (Events Guide)
* arenavision.m3u (acestream URLs)


